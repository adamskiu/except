#include <stdio.h>
#include <stdlib.h>
#include "except.h"
#include "assert.h"

Except_T Allocate_Failed = {"Allocation failed"};

void *allocate(long unsigned n);

int main() {
    extern Except_T Allocate_Failed;
    char *buf;
    TRY
        buf = allocate(99096000000);
    EXCEPT(Allocate_Failed)
        fprintf(stderr, "couldn't allocate the buffer\n");
        exit(EXIT_FAILURE);
    ELSE
        fprintf(stderr, "unknown exception occurred\n");
        exit(EXIT_FAILURE);
    END_TRY;

    return 0;
}

void *allocate(long unsigned n) {
    void *new = malloc(n);

    if (new)
        return new;
    RAISE(Allocate_Failed);
    assert(0);
}